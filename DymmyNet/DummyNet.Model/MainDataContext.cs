﻿namespace DummyNet.Model
{
    using System.Data.Entity;

    using DummyNet.Common.Entities;

    /// <summary>
    /// Главный дата контекст приложения
    /// </summary>
    internal class MainDataContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="MainDataContext"/>.
        /// </summary>
        public MainDataContext()
            : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        /// Получает или задает список пользователей
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Получает или задает коллекцию контента
        /// </summary>
        public DbSet<Content> Content { get; set; }

        /// <summary>
        /// Получает или задает коллекцию постов
        /// </summary>
        public DbSet<Post> Posts { get; set; }

        /// <summary>
        /// Получает или задает коллекцию отношений
        /// </summary>
        public DbSet<Relationship> Relationships { get; set; } 

        public DbSet<Conversataion> Conversations { get; set; }

        public DbSet<Message> Messages { get; set; }
        
        /// <summary>
        /// Создание схемы БД
        /// </summary>
        /// <param name="modelBuilder">Билдер базы</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(o => o.Content).WithRequired(c => c.Owner);
            
            modelBuilder.Entity<Relationship>().HasRequired(c => c.Source).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Relationship>().HasRequired(c => c.Destination).WithMany().WillCascadeOnDelete(false);

            modelBuilder.Entity<Conversataion>().HasMany(c => c.Participants).WithMany(u => u.Conversations).Map(cs =>
                {
                    cs.MapLeftKey("ConvId");
                    cs.MapRightKey("UserId");
                    cs.ToTable("ConversationUser");
                });
            base.OnModelCreating(modelBuilder);
        }
    }
}
