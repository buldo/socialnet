﻿namespace DummyNet.Model
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using Common;

    /// <summary>
    /// Реализация инициализации базы данных
    /// </summary>
    public class DbInitializer
    {
        /// <summary>
        /// Инициализация хранилища
        /// </summary>
        public void Initialize()
        {
            Database.SetInitializer<MainDataContext>(null);

            using (var context = new MainDataContext())
                {
                    if (!context.Database.Exists())
                    {
                        // Создание базы данных SimpleMembership без схемы миграции Entity Framework
                        ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                    }
                }
            
        }
    }
}
