﻿namespace DummyNet.Model
{
    using System.Collections.Generic;
    using System.Linq;

    using DummyNet.Common;
    using DummyNet.Common.Entities;

    /// <summary>
    /// Реализация менеджера сообщений
    /// </summary>
    public class ConversationManager : IConversationManager
    {
        /// <summary>
        /// Получить список разговоров для пользователя
        /// </summary>
        /// <param name="nick">
        /// Ник пользователя
        /// </param>
        /// <returns>
        /// Коллекция разговоров
        /// </returns>
        public IEnumerable<Conversataion> GetConversataionsForUser(string nick)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.Include("Conversations.Participants").FirstOrDefault(u => u.Nick == nick);
                if (user != null)
                {
                    return user.Conversations.ToList();
                }
            }

            return new List<Conversataion>();
        }

        /// <summary>
        /// Получить список разговоров для пользователя
        /// </summary>
        /// <param name="id">
        /// Идентификатор пользователя
        /// </param>
        /// <returns>
        /// Коллекция разговоров
        /// </returns>
        public IEnumerable<Conversataion> GetConversataionsForUser(int id)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.Include("Conversations.Participants").FirstOrDefault(u => u.UserId == id);
                if (user != null)
                {
                    return user.Conversations.ToList();
                }
            }

            return new List<Conversataion>();
        }

        /// <summary>
        /// Получить список сообщений для разговора
        /// </summary>
        /// <param name="conversationId">
        /// Идентификатор разговора
        /// </param>
        /// <returns>
        /// Сообщения в разговоре
        /// </returns>
        public IEnumerable<Message> GetMessagesForConversation(int conversationId)
        {
            using (var context = new MainDataContext())
            {
                var conversation =
                    context.Conversations.Include("Messages.Owner")
                        .FirstOrDefault(o => o.ConversationId == conversationId);
                if (conversation != null)
                {
                    return conversation.Messages;
                }
                else
                {
                    return new List<Message>();
                }
            }
        }

        /// <summary>
        /// Создание разговора
        /// </summary>
        /// <param name="firstNick">
        /// Ник первого собеседника
        /// </param>
        /// <param name="secondNick">
        /// Ник второго собеседники
        /// </param>
        public void CreateNewConversation(string firstNick, string secondNick)
        {
            using (var context = new MainDataContext())
            {
                var firstUser = context.Users.FirstOrDefault(o => o.Nick == firstNick);
                var secondUser = context.Users.FirstOrDefault(o => o.Nick == secondNick);
                if (firstUser != null && secondUser != null)
                {
                    var conversation =
                        context.Conversations.FirstOrDefault(
                            conv =>
                            conv.Participants.Any(o => o.UserId == firstUser.UserId)
                            && conv.Participants.Any(o => o.UserId == secondUser.UserId));
                    if (conversation == null)
                    {
                        conversation = context.Conversations.Create();
                        conversation.Participants.Add(firstUser);
                        conversation.Participants.Add(secondUser);
                        context.Conversations.Add(conversation);
                        context.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Получение пользователей для разговора
        /// </summary>
        /// <param name="conversationId">
        /// Идентификатор разговора
        /// </param>
        /// <returns>
        /// Пользователи, участвуюшие в разговоре
        /// </returns>
        public IEnumerable<User> GetConversationUsers(int conversationId)
        {
            using (var context = new MainDataContext())
            {
                var conversation =
                    context.Conversations.Include("Participants")
                        .FirstOrDefault(o => o.ConversationId == conversationId);
                if (conversation != null)
                {
                    return conversation.Participants.ToList();
                }
                else
                {
                    return new List<User>();
                }
            }
        }

        /// <summary>
        /// Создание и сохранение нового сообщения
        /// </summary>
        /// <param name="userNick">Ник пользователя, отправляющего сообщение</param>
        /// <param name="conversationId">Идентификатор разговора</param>
        /// <param name="text">Текст сообщения</param>
        /// <returns>
        /// Новое сообщение
        /// </returns>
        public Message SaveMessage(string userNick, int conversationId, string text)
        {
            using (var context = new MainDataContext())
            {
                var conversation =
                    context.Conversations.Include("Participants")
                        .FirstOrDefault(o => o.ConversationId == conversationId);
                
                if (conversation != null)
                {
                    var user = conversation.Participants.FirstOrDefault(u => u.Nick == userNick);
                    if (user != null)
                    {
                        var message = context.Messages.Create();
                        message.Text = text;
                        message.Owner = user;
                        context.Messages.Add(message);
                        conversation.Messages.Add(message);
                        context.SaveChanges();
                        return message;
                    }
                }
            }

            return null;
        }
    }
}