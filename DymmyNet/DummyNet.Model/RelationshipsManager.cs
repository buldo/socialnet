﻿namespace DummyNet.Model
{
    using System.CodeDom;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using DummyNet.Common;
    using DummyNet.Common.Entities;

    /// <summary>
    /// Реализация менеджера отношений
    /// </summary>
    public class RelationshipsManager : IRelationshipsManager
    {
        /// <summary>
        /// Найти тех, на кого подписан пользователь
        /// </summary>
        /// <param name="nick">Ник пользователя</param>
        /// <returns>Список тех, на кого подписан пользователь</returns>
        public IEnumerable<User> FindSubscriptions(string nick)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.FirstOrDefault(o => o.Nick == nick);
                if (user != null)
                {
                    var users = context.Relationships.Where(o => o.Source.Nick == user.Nick).Select(o => o.Destination).Include("Avatar").ToList();
                    return users;
                }
            }

            return new List<User>();
        }

        /// <summary>
        /// Создать односторониие отношение между пользователями
        /// </summary>
        /// <param name="source">Ник подписчика</param>
        /// <param name="destination">Тот на кого подписываешься</param>
        public void CreateRelationship(string source, string destination)
        {
            using (var context = new MainDataContext())
            {
                var sourceUser = context.Users.FirstOrDefault(o => o.Nick == source);
                var destinationUser = context.Users.FirstOrDefault(o => o.Nick == destination);

                if (sourceUser != null && destinationUser != null)
                {
                    var newReation = new Relationship { Source = sourceUser, Destination = destinationUser };
                    context.Relationships.Add(newReation);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Проверка подписан ли один пользователь на другого
        /// </summary>
        /// <param name="source">Источник подписки</param>
        /// <param name="destination">Назначение подписки</param>
        /// <returns></returns>
        public bool CheckRelationship(string source, string destination)
        {
            using (var context = new MainDataContext())
            {
                var sourceUser = context.Users.FirstOrDefault(o => o.Nick == source);
                var destinationUser = context.Users.FirstOrDefault(o => o.Nick == destination);

                if (sourceUser != null && destinationUser != null)
                {
                    return context.Relationships.Any(o => o.Source.UserId == sourceUser.UserId && o.Destination.UserId == destinationUser.UserId);
                }

                return false;
            }
        }
    }
}
