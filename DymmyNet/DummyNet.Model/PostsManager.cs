﻿namespace DummyNet.Model
{
    using System.Collections.Generic;
    using System.Linq;

    using Common;
    using Common.Entities;

    /// <summary>
    /// Реализация менеджера постов
    /// </summary>
    public class PostsManager : IPostsManager
    {
        /// <summary>
        /// Получить посты для пользователя
        /// </summary>
        /// <param name="userNick">Ник пользователя</param>
        /// <returns>Список всех постов пользователя</returns>
        public List<Post> GetPostsForUser(string userNick)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.FirstOrDefault(o => o.Nick == userNick);
                if (user != null)
                {
                    var posts = context.Posts.Where(o => o.Owner.UserId == user.UserId).ToList();
                    return posts;
                }

                return new List<Post>();
            }
        }

        /// <summary>
        /// Добавление поста для пользователя
        /// </summary>
        /// <param name="userNick">Ник пользователя</param>
        /// <param name="text">Текст поста</param>
        public void AddPost(string userNick, string text)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.FirstOrDefault(o => o.Nick == userNick);
                if (user != null)
                {
                    var post = new Post { Owner = user, Text = text };
                    context.Posts.Add(post);
                    context.SaveChanges();
                }
            }
        }
    }
}
