﻿namespace DummyNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DummyNet.Common;
    using DummyNet.Common.Entities;

    /// <summary>
    /// Реализация менеджера пользователей
    /// </summary>
    public class UsersManager : IUsersManager
    {
        /// <summary>
        /// Получить пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Найденный пользователь</returns>
        public User GetUser(int id)
        {
            using (var context = new MainDataContext())
            {
                return context.Users.Find(id);
            }
        }

        /// <summary>
        /// Получить пользователя по нику
        /// </summary>
        /// <param name="nick">Ник пользователя</param>
        /// <returns>Найденный пользователь</returns>
        public User GetUser(string nick)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.Include("Avatar").FirstOrDefault(u => u.Nick.ToLower() == nick);
                return user;
            }
        }

        /// <summary>
        /// Поиск пользователей по нескольким строкам
        /// </summary>
        /// <param name="searchStrings">Строки для поиска</param>
        /// <returns>Найденные пользователи</returns>
        public IEnumerable<User> FindUsers(IEnumerable<string> searchStrings)
        {
            using (var context = new MainDataContext())
            {
                var retList = new List<User>();
                foreach (var searchString in searchStrings)
                {
                    var s = searchString;
                    var results =
                        context.Users.Include("Avatar").Where(
                            o => o.Name.Contains(s) || o.Patronymic.Contains(s) || o.Surname.Contains(s));
                    foreach (var result in results)
                    {
                        if (!retList.Contains(result))
                        {
                            retList.Add(result);
                        }
                    }
                }

                return retList;
            }
        }

        /// <summary>
        /// Обновить пользователя
        /// </summary>
        /// <param name="newData">Обновлённые данные пользователя</param>
        public void UpdateUser(User newData)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.Find(newData.UserId);
                user.Name = newData.Name;
                user.Surname = newData.Surname;
                user.Patronymic = newData.Patronymic;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Добавить нового пользователя
        /// </summary>
        /// <param name="user">Новый пользователь</param>
        public void AddUser(User user)
        {
            using (var context = new MainDataContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public void ChangeAvatar(int newAvatarId, string name)
        {
            using (var context = new MainDataContext())
            {
                var user = context.Users.FirstOrDefault(o => o.Nick == name);
                if (user != null)
                {
                    user.Avatar = context.Content.Find(newAvatarId);
                    context.SaveChanges();
                }
            }
        }
    }
}