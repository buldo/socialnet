﻿using System;
using System.IO;
using System.Linq;
using DummyNet.Common;
using DummyNet.Common.Entities;

namespace DummyNet.Model
{
    /// <summary>
    /// Реализация менеджера контента
    /// </summary>
    public class ContentManager : IContentManager
    {
        /// <summary>
        /// Сохранить изображение из Base64 строки
        /// </summary>
        /// <param name="base64String">строка с изображением</param>
        /// <param name="basePath">Базовый путь для хранения на сервере</param>
        /// <param name="ownerNick"></param>
        /// <returns>Id сохранённого контента</returns>
        public int SaveNewJpegFromB64(string base64String, string basePath, string ownerNick)
        {
            using (var context = new MainDataContext())
            {
                var owner = context.Users.FirstOrDefault(o => o.Nick == ownerNick);
                if(owner != null)
                {
                    var fileName = Path.ChangeExtension(Path.GetRandomFileName(), "jpg");
                    File.WriteAllBytes(Path.Combine(basePath, fileName), Convert.FromBase64String(base64String));
                    var newRecord = new Content
                    {
                        Owner = owner,
                        Url = fileName
                    };
                    context.Content.Add(newRecord);
                    context.SaveChanges();
                    return newRecord.ContentId;
                }
                return -1;
            }
        }
    }
}