﻿namespace DummyNet.Common
{
    /// <summary>
    /// Интерфейс менеджера контента
    /// </summary>
    public interface IContentManager
    {
        /// <summary>
        /// Сохранить изображение из Base64 строки
        /// </summary>
        /// <param name="base64String">строка с изображением</param>
        /// <param name="basePath">Базовый путь для хранения на сервере</param>
        /// <param name="ownerNick">Ник хозяина изображения</param>
        /// <returns>Id сохранённого контента</returns>
        int SaveNewJpegFromB64(string base64String, string basePath, string ownerNick);
    }
}