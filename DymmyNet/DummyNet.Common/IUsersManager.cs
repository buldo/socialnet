﻿namespace DummyNet.Common
{
    using System.Collections.Generic;
    using DummyNet.Common.Entities;

    /// <summary>
    /// Интерфейс менеджера пользователей
    /// </summary>
    public interface IUsersManager
    {
        /// <summary>
        /// Получить пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Найденный пользователь</returns>
        User GetUser(int id);
        
        /// <summary>
        /// Получить пользователя по нику
        /// </summary>
        /// <param name="nick">Ник пользователя</param>
        /// <returns>Найденный пользователь</returns>
        User GetUser(string nick);

        /// <summary>
        /// Поиск пользователей по нескольким строкам
        /// </summary>
        /// <param name="searchStrings">Строки для поиска</param>
        /// <returns>Найденный пользователи</returns>
        IEnumerable<User> FindUsers(IEnumerable<string> searchStrings);

        /// <summary>
        /// Обновить пользователя
        /// </summary>
        /// <param name="newData">Обновлённые данные пользователя</param>
        void UpdateUser(User newData);

        /// <summary>
        /// Добавить нового пользователя
        /// </summary>
        /// <param name="user">Новый пользователь</param>
        void AddUser(User user);

        void ChangeAvatar(int newAvatarId, string name);
    }
}
