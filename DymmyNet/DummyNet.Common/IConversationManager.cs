﻿namespace DummyNet.Common
{
    using System.Collections.Generic;

    using DummyNet.Common.Entities;

    /// <summary>
    /// Интерфейс менеджера разговоров
    /// </summary>
    public interface IConversationManager
    {
        /// <summary>
        /// Получить список разговоров для пользователя
        /// </summary>
        /// <param name="nick">
        /// Ник пользователя
        /// </param>
        /// <returns>
        /// Коллекция разговоров
        /// </returns>
        IEnumerable<Conversataion> GetConversataionsForUser(string nick);

        /// <summary>
        /// Получить список разговоров для пользователя
        /// </summary>
        /// <param name="id">
        /// Идентификатор пользователя
        /// </param>
        /// <returns>
        /// Коллекция разговоров
        /// </returns>
        IEnumerable<Conversataion> GetConversataionsForUser(int id);

        /// <summary>
        /// Получить список сообщений для разговора
        /// </summary>
        /// <param name="conversationId">
        /// Идентификатор разговора
        /// </param>
        /// <returns>
        /// Сообщения в разговоре
        /// </returns>
        IEnumerable<Message> GetMessagesForConversation(int conversationId);

        /// <summary>
        /// Создание разговора
        /// </summary>
        /// <param name="firstNick">Ник первого собеседника</param>
        /// <param name="secondNick">Ник второго собеседники</param>
        void CreateNewConversation(string firstNick, string secondNick);

        /// <summary>
        /// Получение пользователей для разговора
        /// </summary>
        /// <param name="conversationId">
        /// Идентификатор разговора
        /// </param>
        /// <returns>
        /// Пользователи, участвуюшие в разговоре
        /// </returns>
        IEnumerable<User> GetConversationUsers(int conversationId);

        /// <summary>
        /// Создание и сохранение нового сообщения
        /// </summary>
        /// <param name="userNick">Ник пользователя, отправляющего сообщение</param>
        /// <param name="conversationId">Идентификатор разговора</param>
        /// <param name="text">Текст сообщения</param>
        /// <returns>
        /// Новое сообщение
        /// </returns>
        Message SaveMessage(string userNick, int conversationId, string text);
    }
}