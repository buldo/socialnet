﻿namespace DummyNet.Common.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Класс, описывающий контент, загруженный на сервер
    /// </summary>
    [Table("Content")]
    public class Content
    {
        /// <summary>
        /// Получает или задает идентификатор контента
        /// </summary>
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ContentId { get; set; }

        /// <summary>
        /// Получает или задает владельца контента
        /// </summary>
        public virtual User Owner { get; set; }

        /// <summary>
        /// Получает или задает Url по которому доступен контент
        /// </summary>
        public string Url { get; set; }
    }
}
