﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DummyNet.Common.Entities
{
    /// <summary>
    /// Класс, описывающий сообщение
    /// </summary>
    [Table("Messages")]
    public class Message
    {
        /// <summary>
        /// Получает или задает идентификатор сообщения
        /// </summary>
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }

        /// <summary>
        /// Получает или задает владельца сообщения
        /// </summary>
        [Required]
        public User Owner { get; set; }

        /// <summary>
        /// Получает или задает текст сообщения
        /// </summary>
        [Required]
        public string Text { get; set; }
    }
}