﻿namespace DummyNet.Common.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Класс, описывающий пост пользователя
    /// </summary>
    [Table("Posts")]
    public class Post
    {
        /// <summary>
        /// Получает или задает идентификатор поста
        /// </summary>
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PostId { get; set; }

        /// <summary>
        /// Получает или задает текст поста
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Получает или задает создателся поста
        /// </summary>
        public User Owner { get; set; }
    }
}
