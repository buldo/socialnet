﻿namespace DummyNet.Common.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///     Класс, описывающий разговор
    /// </summary>
    [Table("Conversataions")]
    public class Conversataion
    {
        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="Conversataion" />.
        /// </summary>
        public Conversataion()
        {
            Messages = new List<Message>();
            Participants = new List<User>();
        }

        /// <summary>
        ///     Получает или задает идентификатор разговора
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationId { get; set; }

        /// <summary>
        ///     Получает или задает коллекцию участников разговора
        /// </summary>
        public virtual ICollection<User> Participants { get; set; }

        /// <summary>
        ///     Получает или задает коллекцию сообщений в разговоре
        /// </summary>
        public virtual ICollection<Message> Messages { get; set; }
    }
}