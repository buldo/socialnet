﻿namespace DummyNet.Common.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    /// <summary>
    /// Класс, описывающий отношения между пользователями
    /// </summary>
    [Table("Relationships")]
    public class Relationship
    {
        /// <summary>
        /// Получает или задает ключ отношения
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RelationshipId { get; set; }

        /// <summary>
        /// Получает или задает инициатора отношений
        /// </summary>
        public User Source { get; set; }

        /// <summary>
        /// Получает или задает 
        /// </summary>
        public User Destination { get; set; }
    }
}
