﻿namespace DummyNet.Common.Entities
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Класс, описывающий пользователя
    /// </summary>
    [Table("Users")]
    public class User
    {
        public User()
        {
            Content = new List<Content>();
            Conversations = new List<Conversataion>();
        }

        /// <summary>
        /// Получает или задает идентификатор пользователя
        /// </summary>
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// Получает или задает ник пользователя
        /// </summary>
        public string Nick { get; set; }

        /// <summary>
        /// Получает или задает Имя пользователя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает фамилию пользователя
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Получает или задает отчество пользователя
        /// </summary>
        public string Patronymic { get; set; }
        
        /// <summary>
        /// Получает или задает аватар пользователя
        /// </summary>
        public virtual Content Avatar { get; set; }

        /// <summary>
        /// Получает или задает разговоры, в которых принимает участие пользователь
        /// </summary>
        public virtual ICollection<Conversataion> Conversations { get; set; }

        public virtual ICollection<Content> Content { get; set; } 
    }
}
