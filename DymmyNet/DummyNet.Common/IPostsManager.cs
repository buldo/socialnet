﻿namespace DummyNet.Common
{
    using System.Collections.Generic;
    using Entities;
    
    /// <summary>
    /// Интерфейс менеджера постов
    /// </summary>
    public interface IPostsManager
    {
        /// <summary>
        /// Получить посты для пользователя
        /// </summary>
        /// <param name="userNick">Ник пользователя</param>
        /// <returns>Список всех постов пользователя</returns>
        List<Post> GetPostsForUser(string userNick);

        /// <summary>
        /// Добавление поста для пользователя
        /// </summary>
        /// <param name="userNick">Ник пользователя</param>
        /// <param name="text">Текст поста</param>
        void AddPost(string userNick, string text);
    }
}
