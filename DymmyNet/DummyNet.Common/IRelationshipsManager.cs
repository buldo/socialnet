﻿using System.Security.Cryptography.X509Certificates;

namespace DummyNet.Common
{
    using System.Collections.Generic;

    using DummyNet.Common.Entities;

    /// <summary>
    /// Интерфейс менеджера отношений
    /// </summary>
    public interface IRelationshipsManager
    {
        /// <summary>
        /// Найти тех, на кого подписан пользователь
        /// </summary>
        /// <param name="nick">Ник пользователя</param>
        /// <returns>Список тех, на кого подписан пользователь</returns>
        IEnumerable<User> FindSubscriptions(string nick);

        /// <summary>
        /// Создать односторониие отношение между пользователями
        /// </summary>
        /// <param name="source">Ник подписчика</param>
        /// <param name="destination">Тот на кого подписываешься</param>
        void CreateRelationship(string source, string destination);

        /// <summary>
        /// Проверка подписан ли один пользователь на другого
        /// </summary>
        /// <param name="source">Источник подписки</param>
        /// <param name="destination">Назначение подписки</param>
        /// <returns></returns>
        bool CheckRelationship(string source, string destination);
    }
}
