﻿window.userApp.imViewModel = (function (ko, datacontext) {
    this.hub = $.connection.imHub;
    var im = this.hub;
    var conversations = ko.observableArray(),
        currentConversation = ko.observable(),
        conversationMessages = ko.observableArray(),
        newMessage = ko.observable(),

        sendMessage = function () {
            im.server.sendMessage(newMessage().conversationId, newMessage().text());
            newMessage().text("");
        },
        selectConversation = function(conversation) {
            currentConversation(conversation);
            im.server.getMessages(conversation.conversationId);
            newMessage(new datacontext.message());
            newMessage().conversationId = conversation.conversationId;
            conversation.newMessagesCnt(0);
        };

    this.hub.client.processConversations = function (allConversations) {
        var mappedConversations = $.map(allConversations, function (data) { return new datacontext.conversation(data) });
        conversations(mappedConversations);
    }
    this.hub.client.processMessages = function (allMessages) {
        var reverted = allMessages.reverse();
        var mappedConversations = $.map(reverted, function (data) { return new datacontext.message(data) });
        conversationMessages(mappedConversations);
    }
    this.hub.client.processNewMessage = function(messageData) {
        var message = new datacontext.message(messageData);
        if (currentConversation() && message.conversationId === currentConversation().conversationId) {
            conversationMessages.unshift(message);
        } else {
            conversations().every(function (element, index, array) {
                if (element.conversationId === message.conversationId) {
                    element.newMessagesCnt(element.newMessagesCnt() + 1);
                    return false;
                };
            });
        }
    }


    $.connection.hub.start(function () { im.server.getConversations() });
    
    return {
        conversations: conversations,
        currentConversation: currentConversation,
        conversationMessages: conversationMessages,
        newMessage: newMessage,
        sendMessage: sendMessage,
        selectConversation: selectConversation
    };

})(ko, userApp.datacontext);

// Initiate the Knockout bindings
ko.applyBindings(window.userApp.imViewModel);