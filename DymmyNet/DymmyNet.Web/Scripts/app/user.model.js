﻿(function (ko, datacontext) {
    datacontext.user = user;
    datacontext.post = post;

    function user(data) {
        var self = this;
        data = data || {};

        // Persisted properties
        self.userId = data.userId;
        self.nick = data.nick;
        self.name = ko.observable(data.name);
        self.surname = ko.observable(data.surname);
        self.patronymic = ko.observable(data.patronymic);
        self.posts = ko.observableArray();
        self.isFriend = ko.observable();
        self.avatar = ko.observable(data.avatar);
        
        // Non-persisted properties
        self.errorMessage = ko.observable();

        self.addNewPost = function(newPost) {
            return datacontext.addNewPost(newPost);
        }
        
        self.toJson = function () { return ko.toJSON(self) };
    };

    function post(data) {
        var self = this;
        data = data || {};

        self.nick = data.nick;
        self.text = ko.observable(data.text);

        // Non-persisted properties
        self.errorMessage = ko.observable();

        self.toJson = function () { return ko.toJSON(self) };
    }
})(ko, userApp.datacontext);