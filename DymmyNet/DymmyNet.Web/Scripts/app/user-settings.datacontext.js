﻿window.userApp = window.userApp || {};

window.userApp.datacontext = (function () {

    var datacontext = {
        getUser: getUser,
        createUser: createUser,
        saveChangedUser: saveChangedUser,
        saveChangedAvatar: saveChangedAvatar
    };

    return datacontext;

    function getUser(userObserv, errorObservable) {
        return ajaxRequest("get", userUrl())
            .done(getSucceeded)
            .fail(getFailed);

        function getSucceeded(data) {
            var newUser = new createUser(data);
            userObserv(newUser);
        }

        function getFailed() {
            errorObservable("При получении данных произошла ошибка.");
        }
    }

    function createUser(data) {
        return new datacontext.user(data); // todoList is injected by todo.model.js
    }
    
    function saveChangedUser(user) {
        // clearErrorMessage(user);
        return ajaxRequest("put", saveUserUrl(user.userId), user, "text")
            .success(function() {
                user.errorMessage("Сохранено");
            })
            .fail(function () {
                user.errorMessage("При обновлении данных произошла ошибка.");
            });
    }
    
    function saveChangedAvatar(avatar, statusObs) {
        clearErrorMessage(avatar);
        return ajaxRequest("put", changeAvatarUrl(avatar.userId), avatar)
            .done(function (result) {
                statusObs("Аватар обновлён");
                //avatar.expenseId = result.expenseId;
            })
            .fail(function (e) {
                if (e.status == 200)
                    return; // todo not sure why jQuery is giving an error
                var msg = {};
                if (e.responseText && e.responseText.length > 0) {
                    var message = JSON.parse(e.responseText);
                    msg = message.message || {};
                }
                avatar.errorMessage("Error adding a new avatar item:" + msg);
            });
    }

    // Private
    function clearErrorMessage(entity) { entity.errorMessage(null); }
    function ajaxRequest(type, url, data, dataType) { // Ajax helper
        var options = {
            dataType: dataType || "json",
            contentType: "application/json",
            cache: false,
            type: type,
            data: data ? data.toJson() : null
        };
        var antiForgeryToken = $("#antiForgeryToken").val();
        if (antiForgeryToken) {
            options.headers = {
                'RequestVerificationToken': antiForgeryToken
            }
        }
        return $.ajax(url, options);
    }
    // routes
    function userUrl(id) { return "/api/user/" + (id || ""); }
    function saveUserUrl(id) { return "/api/user/put/" + (id || ""); }
    function changeAvatarUrl(id) { return "/api/user/changeAvatar/" + (id || ""); }


})();