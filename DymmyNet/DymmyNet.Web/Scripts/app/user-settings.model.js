﻿(function (ko, datacontext) {
    datacontext.user = user;

    function user(data) {
        var self = this;
        data = data || {};

        // Persisted properties
        self.userId = data.userId;
        self.nick = data.nick;
        self.name = ko.observable(data.name);
        self.surname = ko.observable(data.surname);
        self.patronymic = ko.observable(data.patronymic);
        self.originalAvatar = data.avatar;
        
        self.image = ko.observable(data.image);
        self.imageType = ko.observable(data.imageType);
        self.imageFile = ko.observable();
        self.imagePath = ko.observable();
        self.imageObjectURL = ko.observable();
        self.imageStatus = ko.observable();

        self.imageSrc = ko.computed(function () {
            return self.imageType() + "," + self.image();
        });

        // Non-persisted properties
        self.errorMessage = ko.observable();

        self.saveChanges = function () {
            return datacontext.saveChangedUser(self);
        };


        self.imageObjectURL.subscribe(function () {
            return datacontext.saveChangedAvatar(new avatarDto(self.userId, self.image(), self.imageType(), self.errorMessage), self.imageStatus);
        });

        



        // Auto-save when these properties change
        //self.isDone.subscribe(saveChanges);
        //self.title.subscribe(saveChanges);

        self.toJson = function () { return ko.toJSON(self) };
    };

    function avatarDto(userId, binImage, imageType, errorMessage) {
        var self = this;

        self.userId = userId;
        self.image = binImage;
        self.imageType = imageType;
        self.errorMessage = errorMessage;

        self.toJson = function () {
            var json = ko.toJSON(self);
            return json;
        };
    }
})(ko, userApp.datacontext);