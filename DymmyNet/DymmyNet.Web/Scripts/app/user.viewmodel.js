﻿window.userApp.userViewModel = (function (ko, datacontext) {
    var user = ko.observable(),
        newPost = ko.observable(),
        posts = ko.observableArray(),
        error = ko.observable(),
        isFriend = ko.observable(),
        addPost = function(_post) {
            datacontext.addNewPost(_post.newPost());
            posts.unshift(_post.newPost());
            newPost(new datacontext.post());
            newPost().nick = userNick;
        },
        addToFriends = function() {
            datacontext.addToFriends(isFriend);
        };

    datacontext.getUser(user, error); // load todoLists
    datacontext.getPosts(posts, error);
    datacontext.checkRelations(isFriend);
    newPost(new datacontext.post());
    newPost().nick = userNick;
    return {
        user: user,
        posts: posts,
        newPost: newPost,
        isFriend: isFriend,
        error: error,
        addPost: addPost,
        addToFriends: addToFriends
    };

})(ko, userApp.datacontext);

// Initiate the Knockout bindings
ko.applyBindings(window.userApp.userViewModel);
