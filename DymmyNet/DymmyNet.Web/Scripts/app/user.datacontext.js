﻿window.userApp = window.userApp || {};

window.userApp.datacontext = (function () {

    var datacontext = {
        getUser: getUser,
        getPosts: getPosts,
        createUser: createUser,
        createPost: createPost,
        addNewPost: addNewPost,
        addToFriends: addToFriends,
        checkRelations: checkRelations
    };

    return datacontext;

    function getUser(userObserv, errorObservable) {
        return ajaxRequest("get", userUrl())
            .done(getSucceeded)
            .fail(getFailed);

        function getSucceeded(data) {
            var newUser = new createUser(data);
            userObserv(newUser);
            ajaxRequest("get", postUrl())
                .done(postsGetSucceeded)
                .fail(postsGetFailed);

            function postsGetSucceeded(postData) {
                var reverseData = postData.reverse();
                var mappedPosts = $.map(reverseData, function (list) { return new createPost(list); });
                userObserv().posts(mappedPosts);
            }

            function postsGetFailed(errorData) {
                errorObservable("При получении данных произошла ошибка.");
            }

        }

        function getFailed(errorData) {
            errorObservable("При получении данных произошла ошибка.");
        }
    }

    function getPosts(postsObserv, errorObservable) {
        return ajaxRequest("get", postUrl())
            .done(postsGetSucceeded)
            .fail(postsGetFailed);

        function postsGetSucceeded(postData) {
            var reverseData = postData.reverse();
            var mappedPosts = $.map(reverseData, function(list) { return new createPost(list); });
            postsObserv(mappedPosts);
        }

        function postsGetFailed(errorData) {
            errorObservable("При получении данных произошла ошибка.");
        }
    }

    function createUser(data) {
        return new datacontext.user(data); // todoList is injected by todo.model.js
    }
    
    function createPost(data) {
        return new datacontext.post(data); // todoList is injected by todo.model.js
    }

    function addNewPost(newPost) {
        
        return ajaxRequest("put", postUrl(), newPost, "text")
            .success(function(scsData) {
                newPost.errorMessage("Сохранено");
            })
            .fail(function (errData) {
                newPost.errorMessage("При обновлении данных произошла ошибка.");
            });
    }

    function checkRelations(isFriend) {
        return ajaxRequest("get", relationsUrl() + 'IsFriend/' + userNick)
            .done(getSucceeded);

        function getSucceeded(data) {
            isFriend(data);
        }
    }

    function addToFriends(isFriend) {
        return ajaxRequest("put", relationsUrl() + userNick)
            .done(getSucceeded);

        function getSucceeded(data) {
            isFriend(true);
        }
    }
    
    // Private
    function clearErrorMessage(entity) { entity.errorMessage(null); }
    function ajaxRequest(type, url, data, dataType) { // Ajax helper
        var options = {
            dataType: dataType || "json",
            contentType: "application/json",
            cache: false,
            type: type,
            data: data ? data.toJson() : null
        };
        var antiForgeryToken = $("#antiForgeryToken").val();
        if (antiForgeryToken) {
            options.headers = {
                'RequestVerificationToken': antiForgeryToken
            }
        }
        return $.ajax(url, options);
    }
    // routes
    function userUrl() { return "/api/user/" + (userNick || ""); }
    function relationsUrl() { return "/api/relationship/"; }
    function postUrl() { return "/api/post/" + (userNick || ""); }
})();