﻿window.userApp.userSettingsViewModel = (function (ko, datacontext) {
    var user = ko.observable(),
        error = ko.observable(),
        saveUser = function(user) {
            datacontext.saveChangedUser(user.user());
        },
        saveAvatar = function (user) {
            user.user().saveChangedAvatar();
            //datacontext.saveChangedAvatar();
        },
            uploadPreview = function(files){
            var self = this
            self.loadingPreview(true)
            file = files[0]
            if(file.size > 20000000){
                alert("that image is a bit too big for us, got anything smaller?")
            }
            console.log("file size " + file.size)
            var formData = new FormData();
            formData.append("img", file)
            $.ajax({
                url: "/YOUR_UPLOAD_URL",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(data){
                if(data === "error"){
                    alert("somethign wrong happened, please refresh the page")
                    return
                }
                self.imgUrl(data.img_url)
                self.imgId(data.img_id)
            }).fail(function(){
                self.loadingPreview(false)
            })
        };

    datacontext.getUser(user, error); // load todoLists

    return {
        user: user,
        error: error,
        saveUser: saveUser,
        saveAvatar: saveAvatar,
        uploadPreview: uploadPreview
    };

})(ko, userApp.datacontext);

// Initiate the Knockout bindings
ko.applyBindings(window.userApp.userSettingsViewModel);
