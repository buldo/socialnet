﻿(function (ko, datacontext) {
    datacontext.conversation = conversation;
    datacontext.message = message;

    function conversation(data) {
        var self = this;
        data = data || {};

        // Persisted properties
        self.conversationId = data.conversationId;
        self.userName = data.userName;
        self.newMessagesCnt = ko.observable(0);
    };

    function message(data) {
        var self = this;
        data = data || {};

        self.userName = data.userName;
        self.conversationId = data.conversationId;
        self.text = ko.observable(data.text || "");
        
        self.toJson = function () { return ko.toJSON(self) };
    }
})(ko, userApp.datacontext);