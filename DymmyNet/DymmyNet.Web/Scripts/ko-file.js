﻿var windowURL = window.URL || window.webkitURL;
ko.bindingHandlers.file = {
    init: function (element, valueAccessor) {
        $(element).change(function () {
            var file = this.files[0];
            if (ko.isObservable(valueAccessor())) {
                valueAccessor()(file);
            }
        });
    },

    update: function (element, valueAccessor, allBindingsAccessor) {
        var file = ko.utils.unwrapObservable(valueAccessor());
        var bindings = allBindingsAccessor();

        if (bindings.imageBase64 && ko.isObservable(bindings.imageBase64)) {
            if (!file) {
                bindings.imageBase64(null);
                bindings.imageType(null);
            } else {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var result = e.target.result || {};
                    var resultParts = result.split(",");
                    if (resultParts.length === 2) {
                        bindings.imageBase64(resultParts[1]);
                        bindings.imageType(resultParts[0]);
                    }

                    //Now update fileObjet, we do this last thing as implementation detail, it triggers post
                    if (bindings.fileObjectURL && ko.isObservable(bindings.fileObjectURL)) {
                        var oldUrl = bindings.fileObjectURL();
                        if (oldUrl) {
                            windowURL.revokeObjectURL(oldUrl);
                        }
                        bindings.fileObjectURL(file && windowURL.createObjectURL(file));
                    }
                };
                reader.readAsDataURL(file);
            }
        }
    }
};
ko.bindingHandlers.img = {
    update: function (element, valueAccessor) {
        //grab the value of the parameters, making sure to unwrap anything that could be observable
        var value = ko.utils.unwrapObservable(valueAccessor()),
            src = ko.utils.unwrapObservable(value.src),
            fallback = ko.utils.unwrapObservable(value.fallback),
            $element = $(element);

        //now set the src attribute to either the bound or the fallback value
        if (src !== "data:image/jpg;base64,null") {
            $element.attr("src", src);
        } else {
            $element.attr("src", fallback);
        }
    },
    init: function (element, valueAccessor) {
        var $element = $(element);
        var value = ko.utils.unwrapObservable(valueAccessor());
        var busy = ko.utils.unwrapObservable(value.busy);

        $element.attr("src", busy);

        //hook up error handling that will unwrap and set the fallback value
        $element.error(function () {


            $element.attr("src", busy);
        });
    },
};