﻿using System.Web;
using System.Web.Optimization;

namespace DymmyNet.Web
{
    public class BundleConfig
    {
        // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.debug.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajaxlogin").Include(
                "~/Scripts/app/ajaxlogin.js"));

            bundles.Add(new ScriptBundle("~/bundles/todo").Include(
                "~/Scripts/app/common.bindings.js",
                "~/Scripts/app/todo.datacontext.js",
                "~/Scripts/app/todo.model.js",
                "~/Scripts/app/todo.viewmodel.js"));

            bundles.Add(new ScriptBundle("~/bundles/userSettings").Include(
                "~/Scripts/ko-file.js",
                "~/Scripts/app/common.bindings.js",
                "~/Scripts/app/user-settings.datacontext.js",
                "~/Scripts/app/user-settings.model.js",
                "~/Scripts/app/user-settings.viewmodel.js"));

            bundles.Add(new ScriptBundle("~/bundles/user").Include(
                "~/Scripts/app/common.bindings.js",
                "~/Scripts/app/user.datacontext.js",
                "~/Scripts/app/user.model.js",
                "~/Scripts/app/user.viewmodel.js"));

            bundles.Add(new ScriptBundle("~/bundles/im").Include(
                "~/Scripts/app/common.bindings.js",
                "~/Scripts/app/im.datacontext.js",
                "~/Scripts/app/im.model.js",
                "~/Scripts/app/im.viewmodel.js"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                // "~/Content/Site.css",
                // "~/Content/TodoList.css",
                "~/Content/bootstrap.min.css"));
            
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.min.js"));
        }
    }
}