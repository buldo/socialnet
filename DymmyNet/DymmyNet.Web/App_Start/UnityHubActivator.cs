﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Json;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DymmyNet.Web.App_Start.UnityHubActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(DymmyNet.Web.App_Start.UnityHubActivator), "Shutdown")]

namespace DymmyNet.Web.App_Start
{

    public class UnityHubActivator
    {
        public static void Start()
        {
            GlobalHost.DependencyResolver.Register(typeof (IHubActivator),
                () => new MyHubActivator(UnityConfig.GetConfiguredContainer()));
        }

        /// <summary>Disposes the Unity container when the application is shut down.</summary>
        public static void Shutdown()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }

        private class MyHubActivator : IHubActivator
        {
            private readonly IUnityContainer _container;

            public MyHubActivator(IUnityContainer container)
            {
                _container = container;
            }

            public IHub Create(HubDescriptor descriptor)
            {
                if (descriptor == null)
                {
                    throw new ArgumentNullException("descriptor");
                }

                if (descriptor.HubType == null)
                {
                    return null;
                }

                object hub = _container.Resolve(descriptor.HubType) ?? Activator.CreateInstance(descriptor.HubType);
                return hub as IHub;
            }
        }
    }
}