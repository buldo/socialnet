﻿namespace DymmyNet.Web.Hubs
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using DummyNet.Common;
    using DummyNet.Common.Entities;

    using DymmyNet.Web.ViewModels;

    using Microsoft.AspNet.SignalR;

    /// <summary>
    /// Хаб для сообщений
    /// </summary>
    [Authorize]
    public class ImHub : Hub
    {
        /// <summary>
        /// Менеджер сообщений
        /// </summary>
        private readonly IConversationManager conversationManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ImHub"/>.
        /// </summary>
        /// <param name="conversationManager">
        /// The conversation manager.
        /// </param>
        public ImHub(IConversationManager conversationManager)
        {
            this.conversationManager = conversationManager;
        }

        /// <summary>
        /// The get conversations.
        /// </summary>
        public void GetConversations()
        {
            var conversations = conversationManager.GetConversataionsForUser(Context.User.Identity.Name).ToList();
            var viewModels = new List<ConversationViewModel>();
            foreach (var conversation in conversations)
            {
                var opponent = conversation.Participants.First(o => o.Nick != Context.User.Identity.Name);
                viewModels.Add(
                    new ConversationViewModel()
                        {
                            ConversationId = conversation.ConversationId, 
                            UserName = opponent.Name + " " + opponent.Surname
                        });
            }

            Clients.Caller.ProcessConversations(viewModels);
        }

        /// <summary>
        /// Отправка сообщений
        /// </summary>
        /// <param name="conversationId">
        /// The conversation id.
        /// </param>
        /// <param name="text">
        /// The text.
        /// </param>
        public void SendMessage(int conversationId, string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            var users = conversationManager.GetConversationUsers(conversationId);
            var list = users as IList<User> ?? users.ToList();
            if (list.Any(o => o.Nick == Context.User.Identity.Name))
            {
                var newMessage = conversationManager.SaveMessage(Context.User.Identity.Name, conversationId, text);
                var messageVm = new MessageViewModel()
                                    {
                                        Text = text, 
                                        UserName =
                                            newMessage.Owner.Name + " " + newMessage.Owner.Surname, 
                                        ConversationId = conversationId
                                    };

                foreach (var user in list)
                {
                    Clients.Group(user.Nick).ProcessNewMessage(messageVm);
                }
            }
        }

        /// <summary>
        /// The get messages.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void GetMessages(int id)
        {
            var messages = conversationManager.GetMessagesForConversation(id);
            var vms =
                messages.Select(
                    o =>
                        new MessageViewModel()
                        {
                            MessageId = o.MessageId, 
                            UserName = o.Owner.Name + " " + o.Owner.Name, 
                            Text = o.Text
                        });

            Clients.Caller.ProcessMessages(vms);
        }

        public override Task OnConnected()
        {
            var name = Context.User.Identity.Name;
            Groups.Add(Context.ConnectionId, name);
            return base.OnConnected();
        }
    }
}