﻿namespace DymmyNet.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using DummyNet.Common;
    using DummyNet.Common.Entities;

    using ViewModels;

    /// <summary>
    /// Контроллер для работы с постами пользователей
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class PostController : ApiController
    {
        /// <summary>
        /// Менеджер постов
        /// </summary>
        private readonly IPostsManager postsManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PostController"/>.
        /// </summary>
        /// <param name="postsManager">
        /// The posts manager.
        /// </param>
        public PostController(IPostsManager postsManager)
        {
            this.postsManager = postsManager;
        }

        /// <summary>
        /// Получение постов пользователя
        /// </summary>
        /// <param name="id">
        /// Имя пользоватеоя
        /// </param>
        /// <returns>
        /// Все посты пользователя
        /// </returns>
        public IEnumerable<PostViewModel> Get(string id)
        {
            var ret = new List<PostViewModel>();
            ret.AddRange(postsManager.GetPostsForUser(id).Select(o => new PostViewModel(o)));
            return ret;
        }

        /// <summary>
        /// Получение всех постов текущего пользователя
        /// </summary>
        /// <returns>
        /// Все посты пользователя
        /// </returns>
        public IEnumerable<Post> Get()
        {
            var ret = new List<Post>();
            ret.AddRange(postsManager.GetPostsForUser(User.Identity.Name));
            return ret;
        }

        /// <summary>
        /// Добавление нового поста
        /// </summary>
        /// <param name="id">Имя пользователя</param>
        /// <param name="value">Содержание поста</param>
        public void Put(string id, [FromBody]PostViewModel value)
        {
            postsManager.AddPost(value.Nick, value.Text);
        }
    }
}
