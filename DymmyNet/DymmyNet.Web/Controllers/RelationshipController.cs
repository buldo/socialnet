﻿namespace DymmyNet.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http;

    using DummyNet.Common;

    using DymmyNet.Web.ViewModels;

    /// <summary>
    /// Контроллер отношений между пользователями
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class RelationshipController : ApiController
    {
        /// <summary>
        /// Менеджер отношений между пользователями
        /// </summary>
        private readonly IRelationshipsManager relationshipManager;

        /// <summary>
        /// Менеджер разговоров
        /// </summary>
        private readonly IConversationManager conversationManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="RelationshipController"/>.
        /// </summary>
        /// <param name="relationshipManager">
        /// The relationship manager.
        /// </param>
        /// <param name="conversationManager">
        /// The conversation Manager.
        /// </param>
        public RelationshipController(IRelationshipsManager relationshipManager, IConversationManager conversationManager)
        {
            this.relationshipManager = relationshipManager;
            this.conversationManager = conversationManager;
        }

        /// <summary>
        /// Получить список того, на кого пользователь подписался
        /// </summary>
        /// <returns>Список пользователей</returns>
        public IEnumerable<UserViewModel> Get()
        {
            return Get(User.Identity.Name);
        }

        /// <summary>
        /// Получить список того, на кого пользователь подписался
        /// </summary>
        /// <param name="id">
        /// Ник пользователя
        /// </param>
        /// <returns>
        /// Список пользователей
        /// </returns>
        public IEnumerable<UserViewModel> Get(string id)
        {
            var collection = relationshipManager.FindSubscriptions(id).Select(o => new UserViewModel(o)).ToList();
            ReplaceAvatarUrl(collection);
            return collection;
        }

        /// <summary>
        /// Проверка является ли человек другом
        /// </summary>
        /// <param name="id">Ник проверяемого</param>
        /// <returns>Являются ли пользователя друзьями</returns>
        [HttpGet]
        public bool IsFriend(string id)
        {
            return relationshipManager.CheckRelationship(User.Identity.Name, id);
        }

        /// <summary>
        /// Добавления отношения с пользователем
        /// </summary>
        /// <param name="id">
        /// Пользователь с которым необходимо установить отношения
        /// </param>
        public void Put(string id)
        {
            if (!relationshipManager.CheckRelationship(User.Identity.Name, id))
            {
                relationshipManager.CreateRelationship(User.Identity.Name, id);
                conversationManager.CreateNewConversation(User.Identity.Name, id);
            }
        }

        /// <summary>
        /// Замена url аватара на полный
        /// </summary>
        /// <param name="collection">Коллекция пользователей</param>
        private void ReplaceAvatarUrl(IEnumerable<UserViewModel> collection)
        {
            foreach (var vm in collection)
            {
                vm.Avatar = string.Format(
                    "{0}://{1}/UsersContent/{2}",
                    HttpContext.Current.Request.Url.Scheme,
                    HttpContext.Current.Request.Url.Authority,
                    vm.Avatar);
            }
        }
    }
}
