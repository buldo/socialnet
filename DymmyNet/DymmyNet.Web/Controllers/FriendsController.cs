﻿namespace DymmyNet.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using DummyNet.Common;
    using ViewModels;

    /// <summary>
    /// Контроллер страницы друзей
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class FriendsController : Controller
    {
        /// <summary>
        /// Менеджер отношений между пользователями
        /// </summary>
        private readonly IRelationshipsManager relationshipsManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="FriendsController"/>.
        /// </summary>
        /// <param name="relationshipsManager">
        /// The relationships manager.
        /// </param>
        public FriendsController(IRelationshipsManager relationshipsManager)
        {
            this.relationshipsManager = relationshipsManager;
        }

        /// <summary>
        /// Отображенние всех друзей
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            ViewBag.MyNick = User.Identity.Name;
            var vm = new UsersListViewModel(relationshipsManager.FindSubscriptions(User.Identity.Name));
            ReplaceAvatarUrl(vm.Users);
            return View(vm);
        }

        /// <summary>
        /// Изменение Url аватара на полный адрес
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        private void ReplaceAvatarUrl(IEnumerable<UserViewModel> collection)
        {
            foreach (var vm in collection)
            {
                vm.Avatar = string.Format(
                    "{0}://{1}/UsersContent/{2}",
                    Request.Url.Scheme,
                    Request.Url.Authority,
                    vm.Avatar);
            }
        }
    }
}
