﻿namespace DymmyNet.Web.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Контроллер для отобажения страницы пользователя
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// The index.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.UserNick = id ?? User.Identity.Name;
                ViewBag.MyNick = User.Identity.Name;
            }
            else
            {
                ViewBag.UserNick = string.Empty;
                ViewBag.MyNick = string.Empty;
            }

            return View();
        }
    }
}