﻿namespace DymmyNet.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using DummyNet.Common;

    using DymmyNet.Web.ViewModels;

    /// <summary>
    /// Контроллер сообщений
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class IMController : Controller
    {
        /// <summary>
        /// Менеджер разговоров
        /// </summary>
        private readonly IConversationManager conversationManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="IMController"/>.
        /// </summary>
        /// <param name="conversationManager">
        /// The conversation manager.
        /// </param>
        public IMController(IConversationManager conversationManager)
        {
            this.conversationManager = conversationManager;
        }

        /// <summary>
        /// Просто отдадим список разговоров
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var viewModels = new List<ConversationViewModel>();
            return View(viewModels);
        }
    }
}
