﻿namespace DymmyNet.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;
    using System.Web.Mvc;

    using DummyNet.Common;

    using DymmyNet.Web.ViewModels;

    /// <summary>
    /// Контроллер поиска
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class SearchController : Controller
    {
        /// <summary>
        /// Менеджер пользователей
        /// </summary>
        private readonly IUsersManager usersManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="SearchController"/>.
        /// </summary>
        /// <param name="usersManager">
        /// The users manager.
        /// </param>
        public SearchController(IUsersManager usersManager)
        {
            this.usersManager = usersManager;
        }

        /// <summary>
        /// Поиск пользователя
        /// </summary>
        /// <param name="searchstring">
        /// The searchstring.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index([FromUri]string searchstring)
        {
            if (searchstring != null)
            {
                var users = new UsersListViewModel(usersManager.FindUsers(searchstring.Split(' ')));
                ReplaceAvatarUrl(users.Users);
                return View(users);
            }
            else
            {
                return View(new UsersListViewModel());
            }
        }

        /// <summary>
        /// Замена url аватара на полный
        /// </summary>
        /// <param name="collection">Коллекция пользователей</param>
        private void ReplaceAvatarUrl(IEnumerable<UserViewModel> collection)
        {
            foreach (var vm in collection)
            {
                vm.Avatar = string.Format(
                    "{0}://{1}/UsersContent/{2}",
                    Request.Url.Scheme,
                    Request.Url.Authority,
                    vm.Avatar);
            }
        }
    }
}
