﻿namespace DymmyNet.Web.Controllers
{
    using System.Web;
    using System.Web.Http;

    using DummyNet.Common;

    using DymmyNet.Web.ViewModels;

    /// <summary>
    /// Контроллер пользователей
    /// </summary>
    [System.Web.Mvc.Authorize]
    public class UserController : ApiController
    {
        /// <summary>
        /// Менеджер пользователей
        /// </summary>
        private readonly IUsersManager usersManager;

        /// <summary>
        /// The content manager.
        /// </summary>
        private readonly IContentManager contentManager;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UserController"/>.
        /// </summary>
        /// <param name="usersManager">
        /// The users manager.
        /// </param>
        /// <param name="contentManager">
        /// The content manager.
        /// </param>
        public UserController(IUsersManager usersManager, IContentManager contentManager)
        {
            this.usersManager = usersManager;
            this.contentManager = contentManager;
        }

        /// <summary>
        /// Получает текущего пользователя
        /// </summary>
        /// <returns>
        /// The <see cref="UserSettingsViewModel"/>.
        /// </returns>
        public UserSettingsViewModel Get()
        {
            return Get(User.Identity.Name);
        }

        /// <summary>
        /// Получает пользователя
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="UserSettingsViewModel"/>.
        /// </returns>
        public UserSettingsViewModel Get(string id)
        {
            var user = usersManager.GetUser(id);
            var vm = user != null ? new UserSettingsViewModel(user) : null;
            if (vm != null)
            {
                if (user.Avatar != null)
                {
                    vm.Avatar = string.Format(
                        "{0}://{1}/UsersContent/{2}", 
                        HttpContext.Current.Request.Url.Scheme, 
                        HttpContext.Current.Request.Url.Authority, 
                        user.Avatar.Url);
                }
                
                return vm;
            }

            return null;
        }

        /// <summary>
        /// Обновление пользователя
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        public void Put(int id, UserSettingsViewModel user)
        {
            usersManager.UpdateUser(user.ToEntity());
        }

        /// <summary>
        /// The change avatar.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="image">
        /// The image.
        /// </param>
        [HttpPut]
        public void ChangeAvatar(string id, [FromBody] ChangeAvatarViewModel image)
        {
            if (image.Image != null)
            {
                if (image.ImageType != null && (image.ImageType.Contains("jpg") || image.ImageType.Contains("jpeg")))
                {
                    var newAvatarId = contentManager.SaveNewJpegFromB64(
                        image.Image,
                        HttpContext.Current.Server.MapPath("~/UsersContent/"),
                        User.Identity.Name);
                    usersManager.ChangeAvatar(newAvatarId, User.Identity.Name);
                }
            }
        }
    }
}
