﻿namespace DymmyNet.Web.ViewModels
{
    using DummyNet.Common.Entities;

    /// <summary>
    /// ViewModel пользователя для простого отображения
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UserViewModel"/>.
        /// </summary>
        public UserViewModel()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UserViewModel"/>.
        /// </summary>
        /// <param name="user">
        /// Пользователь с которого генерируется ViewModel
        /// </param>
        public UserViewModel(User user)
        {
            Nick = user.Nick;
            Name = user.Name;
            Surname = user.Surname;
            Patronymic = user.Patronymic;
            var ava = user.Avatar;
            Avatar = ava != null ? user.Avatar.Url : string.Empty;
        }

        /// <summary>
        /// Получает или задает ник пользователя
        /// </summary>
        public string Nick { get; set; }

        /// <summary>
        /// Получает или задает имя пользователя 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает фамилию пользователя
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Получает или задает отчество пользователя
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// Получает или задает адрес аватарата
        /// </summary>
        public string Avatar { get; set; }
    }
}
