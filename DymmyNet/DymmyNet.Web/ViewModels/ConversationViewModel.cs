﻿using Newtonsoft.Json;

namespace DymmyNet.Web.ViewModels
{
    /// <summary>
    /// Модель-представление для 
    /// </summary>
    public class ConversationViewModel
    {
        /// <summary>
        /// Получает и задает идентификатор разговора
        /// </summary>
        [JsonProperty("conversationId")]
        public int ConversationId { get; set; }

        /// <summary>
        /// Получает или задает имя пользователя с которым идёт переписка
        /// </summary>
        [JsonProperty("userName")]
        public string UserName { get; set; }
    }
}