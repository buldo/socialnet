﻿using Newtonsoft.Json;

namespace DymmyNet.Web.ViewModels
{
    /// <summary>
    /// Модель-представления для сообщения
    /// </summary>
    public class MessageViewModel
    {
        /// <summary>
        /// Получает или задает идентификатор сообщения
        /// </summary>
        [JsonProperty("messageId")]
        public int MessageId { get; set; }

        /// <summary>
        /// Получает или задает идентификатор сообщения
        /// </summary>
        [JsonProperty("conversationId")]
        public int ConversationId { get; set; } 

        /// <summary>
        /// Получает или задает идентификатор пользователя, который отправил сообщение
        /// </summary>
        [JsonProperty("userId")]
        public int UserId { get; set; }

        /// <summary>
        /// Получает или задает имя пользователя, который отправил сообщение
        /// </summary>
        [JsonProperty("userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Получает или задает текст сообщения
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}