﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DummyNet.Common.Entities;

namespace DymmyNet.Web.ViewModels
{
    public class UsersListViewModel
    {
        public UsersListViewModel()
        {
            Users = new List<UserViewModel>();
        }

        public UsersListViewModel(IEnumerable<User> friends)
            : this()
        {
            Users.AddRange(friends.Select(o => new UserViewModel(o)));
        }

        public List<UserViewModel> Users { get; set; } 
    }
}