﻿namespace DymmyNet.Web.ViewModels
{
    using DummyNet.Common.Entities;

    /// <summary>
    /// The post view model.
    /// </summary>
    public class PostViewModel
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PostViewModel"/>.
        /// </summary>
        public PostViewModel()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PostViewModel"/>.
        /// </summary>
        /// <param name="post">
        /// The post.
        /// </param>
        public PostViewModel(Post post)
        {
            Nick = post.Owner.Nick;
            Text = post.Text;
        }

        /// <summary>
        /// Gets or sets the nick.
        /// </summary>
        public string Nick { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }
    }
}