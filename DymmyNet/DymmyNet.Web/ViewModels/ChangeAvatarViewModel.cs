﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DymmyNet.Web.ViewModels
{
    using System.Net.Http;
    using System.Web.Razor.Text;

    public class ChangeAvatarViewModel
    {
        public int UserId { get; set; }

        public string Image { get; set; }
        public string ImageType { get; set; }
    }
}