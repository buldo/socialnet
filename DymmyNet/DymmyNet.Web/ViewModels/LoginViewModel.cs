using System.ComponentModel.DataAnnotations;

namespace DymmyNet.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "��� ������������")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "������")]
        public string Password { get; set; }

        [Display(Name = "��������� ����")]
        public bool RememberMe { get; set; }
    }
}