﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DummyNet.Common.Entities;

namespace DymmyNet.Web.ViewModels
{
    public class FriendsViewModel
    {
        public FriendsViewModel()
        {
            Friends = new List<UserViewModel>();
        }

        public FriendsViewModel(IEnumerable<User> friends)
            : this()
        {
            Friends.AddRange(friends.Select(o => new UserViewModel(o)));
        }

        public List<UserViewModel> Friends { get; set; } 
    }
}