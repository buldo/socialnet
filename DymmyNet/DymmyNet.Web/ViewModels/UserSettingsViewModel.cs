using System.ComponentModel.DataAnnotations;
using DummyNet.Common.Entities;

namespace DymmyNet.Web.ViewModels
{
    using System.Net;

    public class UserSettingsViewModel
    {
        public UserSettingsViewModel()
        {
        }

        public UserSettingsViewModel(User user)
        {
            UserId = user.UserId;
            Name = user.Name;
            Surname = user.Surname;
            Patronymic = user.Patronymic;
        }

        public int UserId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "���")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "�������")]
        public string Surname { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "��������")]
        public string Patronymic { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "������� ������")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "�������� \"{0}\" ������ ��������� �� ����� {2} ��������.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "����� ������")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "������������� ������")]
        [Compare("NewPassword", ErrorMessage = "����� ������ � ��� ������������� �� ���������.")]
        public string ConfirmPassword { get; set; }

        public string Avatar { get; set; }

        public User ToEntity()
        {
            return new User()
            {
                UserId = UserId,
                Name = Name,
                Surname = Surname,
                Patronymic = Patronymic
            };
        }
    }
}